import { Post } from '@nestjs/common';
import { BadRequestException } from '@nestjs/common';
import { Delete } from '@nestjs/common';
import { Param } from '@nestjs/common';
import { Put } from '@nestjs/common';
import { Body } from '@nestjs/common';
import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

class PostDto {
  userName: string;
  pass: string;
}

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(@Param() test): string {
    console.log(6666, test);
    const random1 = Math.floor(Math.random() * 10);
    if(random1%2===0) throw new BadRequestException('LOI CMNR');
    return this.appService.getHello();
  }

  @Post()
  testPost(@Body() postDto: PostDto): PostDto {
    console.log(11111, postDto);
    // const random1 = Math.floor(Math.random() * 10);
    // if(random1%2===0) throw new BadRequestException('LOI CMNR');
    return postDto;
  }

  @Put(':id/update')
  testPut(@Body() postDto: PostDto): PostDto {
    console.log(11111, postDto);
    return postDto;
  }

  @Delete(':id/delete')
  delete(@Body() postDto: PostDto): PostDto {
    console.log(11111, postDto);
    return postDto;
  }
}
